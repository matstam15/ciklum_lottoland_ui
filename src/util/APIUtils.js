
export const API_BASE_URL = 'http://localhost:8080/rps'

const request = (options) => {
  const headers = new Headers({
    'Content-Type': 'application/json',
    Accept: 'application/json'
  })

  const defaults = { headers }
  options = Object.assign({}, defaults, options)

  return fetch(options.url, options)
    .then(response => response.json().then((json) => {
      if (!response.ok) {
        return Promise.reject(json)
      }
      return json
    }))
}

export function play (payload) {
  return request({
    url: `${API_BASE_URL}/play`,
    method: 'POST',
    body: JSON.stringify(payload)
  })
}
