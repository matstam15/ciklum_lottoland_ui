import React from 'react';
import Adapter from 'enzyme-adapter-react-16';
import {
    shallow,
    configure
} from 'enzyme';
import StatsView from './StatsView'

configure({
    adapter: new Adapter()
});

describe('StatsView', () => {

    test('Rendering the simple component', () => {
        const wrapper = shallow(< StatsView />
        );
        expect(wrapper).toMatchSnapshot();
    });


    test('Rendering and validating UI components', () => {
        const wrapper = shallow(<StatsView />
        );

        expect(wrapper.find('.my-header').text()).toBe('Global Stats');
        expect(wrapper.find('.my-body')).toBeTruthy();

    });

})