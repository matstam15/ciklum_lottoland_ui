import React, { Component } from 'react';

import './HomeView.scss';
import Round from '../components/Round'
import { API_BASE_URL } from '../util/APIUtils';

class HomeView extends Component {

    constructor(props) {
        super(props);
        this.state = {
            options: []
        };
    }

    async componentWillMount() {
        const response = await fetch(API_BASE_URL + '/options');
        const json = await response.json();
        this.setState({ options: json , loaded: true});
    }

    render() {
        const options = this.state.options;

        return (
            <div className='view-container'>
                <div className='my-header text-light bg-dark text-center font-weight-bolder'>
                    Rock, Paper, Scissors... Lets Play!
                </div>
                <div className='my-body'>
                    <Round
                        options={options}
                    />
                </div>
            </div>

        );
    }
}

export default HomeView;