import React, { Component } from 'react';

import './StatsView.scss';
import GlobalStats from '../components/GlobalStats';
import { API_BASE_URL } from '../util/APIUtils';

class StatsView extends Component {

    constructor(props) {
        super(props);
        this.state = {
            stats: {}
        };
    }

    async componentWillMount() {
        const response = await fetch(API_BASE_URL + '/stats');
        const json = await response.json();
        this.setState({ stats: json }, () => console.log('StatsView state updated....'));
    }

    render() {
        const stats = this.state.stats;

        return (
            <div className='view-container'>
                <div className='my-header text-light bg-dark text-center font-weight-bolder'>
                    Global Stats
                </div>
                <div className='my-body'>
                    <GlobalStats stats={stats} />
                </div>
            </div>

        );
    }
}

export default StatsView;