import React from 'react';
import Adapter from 'enzyme-adapter-react-16';
import {
    shallow,
    configure
} from 'enzyme';
import fetch from 'jest-fetch-mock'
import { waitForState } from 'enzyme-async-helpers';

import HomeView from './HomeView'

global.fetch = require('jest-fetch-mock')

configure({
    adapter: new Adapter()
});

describe('HomeView', () => {


    beforeEach(() => {
        fetch.resetMocks()
    })

    test('Rendering the simple component', () => {
        const wrapper = shallow(<HomeView />
        );
        expect(wrapper).toMatchSnapshot();
    });

    test('Rendering and validating UI components', () => {
        const wrapper = shallow(<HomeView />
        );

        expect(wrapper.find('.my-header').text()).toBe('Rock, Paper, Scissors... Lets Play!');
        expect(wrapper.find('.my-body')).toBeTruthy();

    });

    // test('test componentWillMount', async () => {
    //     const wrapper = shallow(<HomeView />
    //     );

    //     expect(wrapper.state().options).toHaveLength(0);

    //     fetch.mockResponseOnce(Promise.resolve({
    //         json: () => {
    //             return JSON.stringify({ options: ['Rock', 'Paper', 'Scissors'] })
    //         }
    //     }));

    //     await waitForState(wrapper, state => state.loaded === true);

    //     expect(wrapper.state().options).toHaveLength(3);

    // });

})