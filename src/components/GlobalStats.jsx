import React, { Component } from 'react';
import { Pie } from 'react-chartjs-2';

import './GlobalStats.scss';

class GlobalStats extends Component {

    render() {
        const rawData = this.props.stats ?
            this.props.stats :
            {
                totalRounds: '0',
                totalWinsPlayerOne: '0',
                totalWinsPlayerTwo: '0',
                totalDraws: '0'
            };

        const chartData = {
            labels: [
                'Player 1 Wins',
                'Player 2 Wins',
                'Draw'
            ],
            datasets: [{
                data: [
                    rawData['totalWinsPlayerOne'],
                    rawData['totalWinsPlayerTwo'],
                    rawData['totalDraws']
                ],
                backgroundColor: [
                    '#FF6384',
                    '#36A2EB',
                    '#FFCE56'
                ],
                hoverBackgroundColor: [
                    '#FF6384',
                    '#36A2EB',
                    '#FFCE56'
                ]
            }]
        };

        return (
            <div className='global-stats-container'>
                <div className='stats-data'>
                    <h4>Total Rounds: {rawData.totalRounds}</h4>
                    <p className='font-weight-lighter'>Player 1 Wins: {rawData.totalWinsPlayerOne}</p>
                    <p className='font-weight-lighter'>Player 2 Wins: {rawData.totalWinsPlayerTwo}</p>
                    <p className='font-weight-lighter'>Draws: {rawData.totalDraws}</p>
                </div>
                <div className='stats-chart'>
                    <Pie data={chartData} />
                </div>
            </div>
        );
    }
}

export default GlobalStats;
