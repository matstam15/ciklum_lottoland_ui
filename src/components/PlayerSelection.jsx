import React, { Component } from 'react';
import './PlayerSelection.scss';


class PayerSelection extends Component {

    renderSelection(option, status, alias) {
        const classNames = option ? 'fa fa-hand-' + option.toLowerCase() + '-o' : 'fa';
        return (<button className={status}>
            <i className={classNames}></i>
            <span>{alias}: {option}</span>
        </button>)
    }

    render() {
        const selection = this.props.selection;
        const status = this.props.status ? this.props.status : '';
        return this.renderSelection(selection, status, this.props.alias);
    }
}

export default PayerSelection;