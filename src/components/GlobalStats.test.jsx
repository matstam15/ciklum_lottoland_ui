import React from 'react';
import Adapter from 'enzyme-adapter-react-16';
import {
    shallow,
    mount,
    configure
} from 'enzyme';
import GlobalStats from './GlobalStats'

jest.mock('react-chartjs-2', () => ({
    Pie: () => null
}));

configure({
    adapter: new Adapter()
});

describe('Global Stats', () => {

    test('Rendering the simple component', () => {
        const wrapper = shallow(<GlobalStats />
        );
        expect(wrapper).toMatchSnapshot();
    });


    test('Rendering the stats', () => {

        const stats = {
            totalRounds: '100',
            totalWinsPlayerOne: '25',
            totalWinsPlayerTwo: '25',
            totalDraws: '50'
        }

        const wrapper = mount(
            <GlobalStats stats={stats} />
        );

        // props
        expect(wrapper.props().stats.totalRounds).toBe(stats.totalRounds);
        expect(wrapper.props().stats.totalWinsPlayerOne).toBe(stats.totalWinsPlayerOne);
        expect(wrapper.props().stats.totalWinsPlayerTwo).toBe(stats.totalWinsPlayerTwo);
        expect(wrapper.props().stats.totalDraws).toBe(stats.totalDraws);

        // ui
        expect(wrapper.find('.global-stats-container').children()).toHaveLength(2);
        expect(wrapper.find('.global-stats-container').childAt(0).hasClass('stats-data')).toBeTruthy();
        expect(wrapper.find('.stats-data').childAt(0).text()).toBe('Total Rounds: ' + stats.totalRounds);
        expect(wrapper.find('.stats-data').childAt(1).text()).toBe('Player 1 Wins: ' + stats.totalWinsPlayerOne);
        expect(wrapper.find('.stats-data').childAt(2).text()).toBe('Player 2 Wins: ' + stats.totalWinsPlayerTwo);
        expect(wrapper.find('.stats-data').childAt(3).text()).toBe('Draws: ' + stats.totalDraws);

        expect(wrapper.find('.global-stats-container').childAt(1).hasClass('stats-chart')).toBeTruthy();
        expect(wrapper.find('.stats-chart').contains(wrapper.find('.chartjs-size-monitor')));

    });

    test('Rendering the stats without props', () => {

        const wrapper = mount(
            <GlobalStats />
        );

        // props
        expect(wrapper.props().stats).toBeUndefined();

        // ui
        expect(wrapper.find('.global-stats-container').children()).toHaveLength(2);
        expect(wrapper.find('.global-stats-container').childAt(0).hasClass('stats-data')).toBeTruthy();
        expect(wrapper.find('.stats-data').childAt(0).text()).toBe('Total Rounds: 0');
        expect(wrapper.find('.stats-data').childAt(1).text()).toBe('Player 1 Wins: 0');
        expect(wrapper.find('.stats-data').childAt(2).text()).toBe('Player 2 Wins: 0');
        expect(wrapper.find('.stats-data').childAt(3).text()).toBe('Draws: 0');

        expect(wrapper.find('.global-stats-container').childAt(1).hasClass('stats-chart')).toBeTruthy();
        expect(wrapper.find('.stats-chart').contains(wrapper.find('.chartjs-size-monitor')));

    });

})