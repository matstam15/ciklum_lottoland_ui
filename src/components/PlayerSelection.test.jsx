import React from 'react';
import Adapter from 'enzyme-adapter-react-16';
import {
    shallow,
    mount,
    configure
} from 'enzyme';
import PlayerSelection from './PlayerSelection'

configure({
    adapter: new Adapter()
});

describe('PlayerSelection', () => {

    test('Rendering the simple component', () => {
        const wrapper = shallow(<PlayerSelection />
        );
        expect(wrapper).toMatchSnapshot();
    });


    test('Rendering the with status Rock', () => {

        const props = {
            selection: 'Rock',
            status: 'win',
            alias: 'Player 1'
        }

        const wrapper = mount(
            <PlayerSelection
                selection={props.selection}
                status={props.status}
                alias={props.alias} />
        );

        // props
        expect(wrapper.props().selection).toBe(props.selection);
        expect(wrapper.props().status).toBe(props.status);
        expect(wrapper.props().alias).toBe(props.alias);

        // ui
        expect(wrapper.find('i').hasClass('fa')).toBeTruthy();
        expect(wrapper.find('i').hasClass('fa-hand-' + props.selection.toLowerCase() + '-o')).toBeTruthy();
        expect(wrapper.find('button').hasClass(props.status)).toBeTruthy();
        expect(wrapper.find('span').text()).toBe(props.alias + ': ' + props.selection);

    });

    test('Rendering the with status Paper and Lost', () => {

        const props = {
            selection: 'Paper',
            status: 'lose',
            alias: 'Player 2'
        }

        const wrapper = mount(
            <PlayerSelection
                selection={props.selection}
                status={props.status}
                alias={props.alias} />
        );

        // props
        expect(wrapper.props().selection).toBe(props.selection);
        expect(wrapper.props().status).toBe(props.status);
        expect(wrapper.props().alias).toBe(props.alias);

        // ui
        expect(wrapper.find('i').hasClass('fa')).toBeTruthy();
        expect(wrapper.find('i').hasClass('fa-hand-' + props.selection.toLowerCase() + '-o')).toBeTruthy();
        expect(wrapper.find('button').hasClass(props.status)).toBeTruthy();
        expect(wrapper.find('span').text()).toBe(props.alias + ': ' + props.selection);

    });

    test('Rendering the with status Scissors and Draw', () => {

        const props = {
            selection: 'Scissors',
            status: 'draw',
            alias: 'Player 1'
        }

        const wrapper = mount(
            <PlayerSelection
                selection={props.selection}
                status={props.status}
                alias={props.alias} />
        );

        // props
        expect(wrapper.props().selection).toBe(props.selection);
        expect(wrapper.props().status).toBe(props.status);
        expect(wrapper.props().alias).toBe(props.alias);

        // ui
        expect(wrapper.find('i').hasClass('fa')).toBeTruthy();
        expect(wrapper.find('i').hasClass('fa-hand-' + props.selection.toLowerCase() + '-o')).toBeTruthy();
        expect(wrapper.find('button').hasClass(props.status)).toBeTruthy();
        expect(wrapper.find('span').text()).toBe(props.alias + ': ' + props.selection);

    });


})