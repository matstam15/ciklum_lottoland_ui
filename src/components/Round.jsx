import React, { Component } from 'react';
import PlayerSelection from './PlayerSelection'
import MatchStats from './MatchStats'
import { play } from '../util/APIUtils';

import './Round.scss';

class Round extends Component {

  constructor(props) {
    super(props);
    this.state = {
      options: [],
      lastResult: null,
      player1: null,
      player2: null,
      result: null,
      rounds: [],
    };

    this.randomOption = this.randomOption.bind(this);
    this.playHandler = this.playHandler.bind(this);
    this.reset = this.reset.bind(this);
    this.translateResult = this.translateResult.bind(this);
    this.resolveStatus = this.resolveStatus.bind(this);
    this.reset = this.reset.bind(this);
  }

  componentWillMount() {
    if (sessionStorage.getItem('appState') != null) {
      const storedState = JSON.parse(sessionStorage.getItem('appState'));
      this.setState(storedState);
    }
  }

  componentWillUnmount() {
    // Should be using something else la redux maybe, but for this scenario, this works
    sessionStorage.setItem('appState', JSON.stringify(this.state));
  }

  componentWillReceiveProps(nextProps) {
    console.log('Round componentWillReceiveProps');
    this.setState({
      options: nextProps.options,
      player1: nextProps.options[0],
      player2: nextProps.options[0]
    });
  }

  renderResultMessage() {
    if (!this.state.rounds || this.state.rounds.length === 0)
      return 'Go ahead, start playing'
    return `Round #${this.state.rounds.length} -  ${this.state.lastResult}`;
  }

  render() {

    const player1 = this.state.player1;
    const player2 = this.state.player2;

    return (
      <div>
        <button type='button'
          onClick={() => this.playHandler()}
          className='btn-play'>
          Play Round
          </button>
        <div className='icons-row'>
          <PlayerSelection
            selection={player1}
            alias='Player 1'
            status={this.resolveStatus('p1')}
          />
          <PlayerSelection
            selection={player2}
            alias='Player 2'
            status={this.resolveStatus('p2')}
          />
        </div>
        <div className='round-result'>{this.renderResultMessage()}</div>
        <div >
          <MatchStats rounds={this.state.rounds} />

          <button type='button'
            onClick={() => this.reset()}
            className='btn-reset'>
            Restart Game
          </button>
        </div>
      </div>
    );
  }

  randomOption() {
    if (this.state.options) {
      let index = Math.floor(Math.random() * Math.floor(this.state.options.length));
      if (index > 2) {
        console.error('Random is not working properly');
        index = 0;
      }
      return this.state.options[index];
    }
    return null
  }

  playHandler() {

    const player1Choice = this.state.player1;
    const player2Choice = this.randomOption();

    play({
      player1: player1Choice,
      player2: player2Choice
    })
      .then(json => {
        let newArray = this.state.rounds.slice();
        newArray.unshift(
          {
            player1: json.player1,
            player2: json.player2,
            result: this.translateResult(json.result),
          }
        );

        let trasnlatedResult = this.translateResult(json.result);
        this.setState({
          player1: json.player1,
          player2: json.player2,
          result: json.result,
          lastResult: trasnlatedResult,
          rounds: newArray
        })
      })
  }

  reset() {
    const defaultValue = this.state.options[0];
    this.setState({
      player1: defaultValue,
      player2: defaultValue,
      result: 0,
      lastResult: '',
      rounds: []

    })
  }

  translateResult(value) {
    if (value === 0) {
      return 'Draw. Play Again'
    } else if (value === 2) {
      return 'Player 2 Wins. Congrats!'
    } else if (value === 1) {
      return 'Player 1 Wins. Congrats!'
    } else {
      console.error('Illegal State ' + value + '. Accepted values are 0,1,2');
      throw new Error('Illegal State ' + value + '. Accepted values are 0,1,2');
    }
  }

  resolveStatus(player) {
    const result = this.state.result ? this.state.result : 0;
    if (result === 0 || result === 1 || result === 2) {
      if (result === 0)
        return 'draw';
      if (player === 'p1')
        return (result === 1) ? 'win' : 'lose';
      if (player === 'p2')
        return (result === 2) ? 'win' : 'lose';
      console.error('Illegal State ' + player + '. Accepted values are p1,p2');
      throw new Error('Illegal State ' + player + '. Accepted values are p1,p2');
    }
    console.error('Illegal State ' + result + '. Accepted resuls are 0,1,2');
    throw new Error('Illegal State ' + result + '. Accepted resuls are 0,1,2');
  }
}

export default Round;
