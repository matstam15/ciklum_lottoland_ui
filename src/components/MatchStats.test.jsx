import React from 'react';
import Adapter from 'enzyme-adapter-react-16';
import {
    shallow,
    mount,
    configure
} from 'enzyme';
import MatchStats from './MatchStats'


configure({
    adapter: new Adapter()
});

describe('Match Stats', () => {

    test('Rendering the simple component', () => {
        const wrapper = shallow(<MatchStats />
        );
        expect(wrapper).toMatchSnapshot();
    });


    test('Rendering the match stats without rounds', () => {

        const wrapper = shallow(
            <MatchStats />
        );

        // ui
        expect(wrapper.find('.stats-container').childAt(0).hasClass('stats-table')).toBeTruthy();
        expect(wrapper.find('table').hasClass('stats-table')).toBeTruthy();


    });

    test('Rendering the match stats with empty rounds', () => {

        const rounds = [];
        const wrapper = shallow(
            <MatchStats rounds={rounds} />
        );

        // ui
        expect(wrapper.find('.stats-container').children()).toHaveLength(1);
        expect(wrapper.find('.stats-container').childAt(0).hasClass('stats-table')).toBeTruthy();
        expect(wrapper.find('table').hasClass('stats-table')).toBeTruthy();

        expect(wrapper.find('tbody').children()).toHaveLength(0);

    });


    test('Rendering the match stats with 5 rounds', () => {

        const rounds = [
            { player1: 'Rock', player2: 'Rock', result: 'Its a draw' },
            { player1: 'Rock', player2: 'Scissors', result: 'Player 1 wins' },
            { player1: 'Scissors', player2: 'Rock', result: 'Player 2 wins' },
            { player1: 'Paper', player2: 'Paper', result: 'Its a draw' },
            { player1: 'Rock', player2: 'Rock', result: 'Its a draw' }
        ];
        const wrapper = shallow(
            <MatchStats rounds={rounds} />
        );

        // ui
        expect(wrapper.find('.stats-container').children()).toHaveLength(1);
        expect(wrapper.find('.stats-container').childAt(0).hasClass('stats-table')).toBeTruthy();
        expect(wrapper.find('table').hasClass('stats-table')).toBeTruthy();


        expect(wrapper.find('tbody').children()).toHaveLength(5);

        for (let index = 0; index < rounds.length; index++) {
            expect(wrapper.find('tbody').childAt(index).childAt(0).text()).toBe(rounds[index].player1);
            expect(wrapper.find('tbody').childAt(index).childAt(1).text()).toBe(rounds[index].player2);
            expect(wrapper.find('tbody').childAt(index).childAt(2).text()).toBe(rounds[index].result);
        }
    });


})