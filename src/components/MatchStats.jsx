import React, { Component } from 'react';

import './MatchStats.scss';

class MatchStats extends Component {f

    renderRound(round, key) {
        if (round) {
        return (
            <tr key={key}>
                <td>{round.player1}</td>
                <td>{round.player2}</td>
                <td>{round.result}</td>
            </tr>
        )
        }
    };

    render() {

        const rounds = this.props.rounds ? this.props.rounds : [];
        return (
            <div className='stats-container'>
                <table className='stats-table'>
                    <thead>
                        <tr className='table-primary'>
                            <th scope='col'>Player 1</th>
                            <th scope='col'>Player 2</th>
                            <th scope='col'>Result</th>
                        </tr>
                    </thead>
                    <tbody>
                        {rounds.map((round, index) => this.renderRound(round, index))}
                    </tbody>
                </table>
            </div>
        );
    }

}

export default MatchStats;
