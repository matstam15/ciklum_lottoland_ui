import React from 'react';
import Adapter from 'enzyme-adapter-react-16';
import {
    shallow,
    mount,
    configure
} from 'enzyme';
import Round from './Round'
import * as apiUtils from '../util/APIUtils'


configure({
    adapter: new Adapter()
});

describe('Round', () => {


    // beforeEach(() => {
    //     fetch.resetMocks()
    // })

    test('Rendering the simple component', () => {
        const wrapper = shallow(<Round />
        );
        expect(wrapper).toMatchSnapshot();
    });


    test('Rendering and validating UI components', () => {
        const wrapper = mount(<Round />
        );

        // state
        expect(wrapper.state().options).toHaveLength(0);
        expect(wrapper.state().lastResult).toBeNull();
        expect(wrapper.state().player1).toBeNull();
        expect(wrapper.state().player2).toBeNull();
        expect(wrapper.state().result).toBeNull();
        expect(wrapper.state().rounds).toHaveLength(0);

        // ui
        const playButton = wrapper.find('.btn-play');
        expect(playButton.text()).toBe('Play Round');

        expect(wrapper.find('.icons-row').children()).toHaveLength(2);
        expect(wrapper.find('.draw').length).toBe(2);

        // const player1 = wrapper.find('.icons-row').childAt(0);
        // expect(player1.hasClass('draw')).toBeTruthy();

        // const player2 = wrapper.find('.icons-row').childAt(1);
        // expect(player2.hasClass('draw')).toBeTruthy();

        expect(wrapper.find('.round-result').text()).toBe('Go ahead, start playing');

        const statsTable = wrapper.find('.stats-table');
        expect(statsTable.find('tbody').children()).toHaveLength(0);

        const buttonReset = wrapper.find('.btn-reset');
        expect(buttonReset.hasClass('btn-reset')).toBeTruthy();
        expect(buttonReset.text()).toBe('Restart Game');

    });


    test('Play a round', () => {
        const wrapper = mount(<Round />
        );

        // state
        expect(wrapper.state().options).toHaveLength(0);
        expect(wrapper.state().lastResult).toBeNull();
        expect(wrapper.state().player1).toBeNull();
        expect(wrapper.state().player2).toBeNull();
        expect(wrapper.state().result).toBeNull();
        expect(wrapper.state().rounds).toHaveLength(0);

        // ui
        const playButton = wrapper.find('.btn-play');
        expect(playButton.text()).toBe('Play Round');

        expect(wrapper.find('.icons-row').children()).toHaveLength(2);
        expect(wrapper.find('.draw').length).toBe(2);

        // const player1 = wrapper.find('.icons-row').childAt(0);
        // expect(player1.hasClass('draw')).toBeTruthy();

        // const player2 = wrapper.find('.icons-row').childAt(1);
        // expect(player2.hasClass('draw')).toBeTruthy();

        expect(wrapper.find('.round-result').text()).toBe('Go ahead, start playing');

        const statsTable = wrapper.find('.stats-table');
        expect(statsTable.find('tbody').children()).toHaveLength(0);

        const buttonReset = wrapper.find('.btn-reset');
        expect(buttonReset.hasClass('btn-reset')).toBeTruthy();
        expect(buttonReset.text()).toBe('Restart Game');

        const options = ['Rock', 'Paper', 'Scissors'];
        wrapper.setState(
            {
                options: options,
                player1: options[0],
                player2: options[0]
            }
        );

        apiUtils.play = jest.fn(() => Promise.resolve(
            { player1: 'Rock', player2: 'Scissors', result: 1 }));

        // updated state
        expect(wrapper.state().options).toHaveLength(3);
        expect(wrapper.state().lastResult).toBeNull();
        expect(wrapper.state().player1).toBe(options[0]);
        expect(wrapper.state().player2).toBe(options[0]);
        expect(wrapper.state().result).toBeNull();
        expect(wrapper.state().rounds).toHaveLength(0);

        playButton.simulate('click');


        expect(wrapper.state().options).toHaveLength(3);


        // TODO: Have a look at this...
        // I am having some kind of issues while updating the state on teh wrapper...    
        // wrapper.update() is not forcing the state update    
        wrapper.update()

        console.log('>>>>>>>>>>');
        console.log(JSON.stringify(wrapper.state()));
        console.log('>>>>>>>>>>');


        // process.nextTick(() => {
        //     wrapper.update() // if you need to assert changes in the DOM resulting from state change
        //     expect(wrapper.state().options).toHaveLength(3);
        //     expect(wrapper.state().lastResult).not.toBeNull();
        //     expect(wrapper.state().player1).toBe(options[0]);
        //     //expect(wrapper.state().player2).toBe(options[0]);  // random value
        //     expect(wrapper.state().result).not.toBeNull();
        //     expect(wrapper.state().rounds).toHaveLength(1);
        //     done()
        // })

        // expect(wrapper.state().options).toHaveLength(3);
        // expect(wrapper.state().lastResult).not.toBeNull();
        // expect(wrapper.state().player1).toBe(options[0]);
        // //expect(wrapper.state().player2).toBe(options[0]);  // random value
        // expect(wrapper.state().result).not.toBeNull();
        // expect(wrapper.state().rounds).toHaveLength(1);

    });


    test('Recover from existing state', () => {
        const wrapper = mount(<Round />
        );

        // state
        expect(wrapper.state().options).toHaveLength(0);
        expect(wrapper.state().lastResult).toBeNull();
        expect(wrapper.state().player1).toBeNull();
        expect(wrapper.state().player2).toBeNull();
        expect(wrapper.state().result).toBeNull();
        expect(wrapper.state().rounds).toHaveLength(0);

        // ui
        const playButton = wrapper.find('.btn-play');
        expect(playButton.text()).toBe('Play Round');

        expect(wrapper.find('.icons-row').children()).toHaveLength(2);
        expect(wrapper.find('.draw').length).toBe(2);

        expect(wrapper.find('.round-result').text()).toBe('Go ahead, start playing');

        const statsTable = wrapper.find('.stats-table');
        expect(statsTable.find('tbody').children()).toHaveLength(0);

        const buttonReset = wrapper.find('.btn-reset');
        expect(buttonReset.hasClass('btn-reset')).toBeTruthy();
        expect(buttonReset.text()).toBe('Restart Game');

        const options = ['Rock', 'Paper', 'Scissors'];
        wrapper.setState(
            {
                options: options,
                player1: options[0],
                player2: options[1],
                result: 2,
                lastResult: 'Player 2 Wins. Congrats!',
                rounds: [{ player1: 'Rock', player2: 'Paper', result: 'Player 2 Wins' }]
            }
        );

        apiUtils.play = jest.fn(() => Promise.resolve(
            { player1: 'Rock', player2: 'Scissors', result: 1 }));

        // updated state
        expect(wrapper.state().options).toHaveLength(3);
        expect(wrapper.state().lastResult).toBe('Player 2 Wins. Congrats!');
        expect(wrapper.state().player1).toBe(options[0]);
        expect(wrapper.state().player2).toBe(options[1]);
        expect(wrapper.state().result).toBe(2);
        expect(wrapper.state().rounds).toHaveLength(1);


        const player1 = wrapper.find('.lose');
        expect(player1.text()).toBe('Player 1: Rock');

        const player2 = wrapper.find('.win');
        expect(player2.text()).toBe('Player 2: Paper');

        expect(statsTable.find('tbody').html()).toBe('<tbody><tr><td>Rock</td><td>Paper</td><td>Player 2 Wins</td></tr></tbody>');

    });

    test('test reset', () => {

        const wrapper = mount(<Round />
        );

        const options = ['Rock', 'Paper', 'Scissors'];
        wrapper.setState(
            {
                options: options,
                player1: options[0],
                player2: options[1],
                result: 2,
                lastResult: 'Player 2 Wins. Congrats!',
                rounds: [{ player1: 'Rock', player2: 'Paper', result: 'Player 2 Wins' }]
            }
        );

        apiUtils.play = jest.fn(() => Promise.resolve(
            { player1: 'Rock', player2: 'Scissors', result: 1 }));

        // updated state
        expect(wrapper.state().options).toHaveLength(3);
        expect(wrapper.state().lastResult).toBe('Player 2 Wins. Congrats!');
        expect(wrapper.state().player1).toBe(options[0]);
        expect(wrapper.state().player2).toBe(options[1]);
        expect(wrapper.state().result).toBe(2);
        expect(wrapper.state().rounds).toHaveLength(1);


        const player1 = wrapper.find('.lose');
        expect(player1.text()).toBe('Player 1: Rock');

        const player2 = wrapper.find('.win');
        expect(player2.text()).toBe('Player 2: Paper');

        const statsTable = wrapper.find('.stats-table');
        expect(statsTable.find('tbody').html()).toBe('<tbody><tr><td>Rock</td><td>Paper</td><td>Player 2 Wins</td></tr></tbody>');

        const buttonReset = wrapper.find('.btn-reset');
        expect(buttonReset.hasClass('btn-reset')).toBeTruthy();
        expect(buttonReset.text()).toBe('Restart Game');

        buttonReset.simulate('click');

        wrapper.update();

        // reseted state
        expect(wrapper.state().options).toHaveLength(3);
        expect(wrapper.state().lastResult).toBe('');
        expect(wrapper.state().player1).toBe(options[0]);
        expect(wrapper.state().player2).toBe(options[0]);
        expect(wrapper.state().result).toBe(0);
        expect(wrapper.state().rounds).toHaveLength(0);

        expect(statsTable.find('tbody').html()).toBe('<tbody></tbody>');


    });


    test('test random method', () => {

        const mockMath = Object.create(global.Math);
        mockMath.random = () => 0.5;
        global.Math = mockMath;

        const wrapper = shallow(<Round />
        );

        const options = ['Rock', 'Paper', 'Scissors'];
        wrapper.setState(
            {
                options: options
            }
        );

        expect(wrapper.instance().randomOption()).toBe(options[1]);
        expect(wrapper.instance().randomOption()).toBe(options[1]);
        expect(wrapper.instance().randomOption()).toBe(options[1]);
        expect(wrapper.instance().randomOption()).toBe(options[1]);


        mockMath.random = () => 0.9;
        global.Math = mockMath;

        expect(wrapper.instance().randomOption()).toBe(options[2]);
        expect(wrapper.instance().randomOption()).toBe(options[2]);
        expect(wrapper.instance().randomOption()).toBe(options[2]);
        expect(wrapper.instance().randomOption()).toBe(options[2]);


        mockMath.random = () => 0.2;
        global.Math = mockMath;

        expect(wrapper.instance().randomOption()).toBe(options[0]);
        expect(wrapper.instance().randomOption()).toBe(options[0]);
        expect(wrapper.instance().randomOption()).toBe(options[0]);
        expect(wrapper.instance().randomOption()).toBe(options[0]);
    });

    test('test translateResult method', () => {
        const wrapper = shallow(<Round />
        );

        expect(wrapper.instance().translateResult(0)).toEqual('Draw. Play Again');
        expect(wrapper.instance().translateResult(1)).toEqual('Player 1 Wins. Congrats!');
        expect(wrapper.instance().translateResult(2)).toEqual('Player 2 Wins. Congrats!');


        let error
        try {
            wrapper.instance().translateResult(-1)
        } catch (e) {
            error = e
        }
        expect(error).toBeInstanceOf(Error)

        try {
            wrapper.instance().translateResult('1')
        } catch (e) {
            error = e
        }
        expect(error).toBeInstanceOf(Error)

        try {
            wrapper.instance().translateResult('uno')
        } catch (e) {
            error = e
        }
        expect(error).toBeInstanceOf(Error)

        try {
            wrapper.instance().translateResult(3)
        } catch (e) {
            error = e
        }
        expect(error).toBeInstanceOf(Error)

    });


    test('test resolveStatus method', () => {
        const wrapper = shallow(<Round />
        );

        wrapper.setState(
            {
                result: 0
            }
        );
        expect(wrapper.instance().resolveStatus('p1')).toEqual('draw');
        expect(wrapper.instance().resolveStatus('p2')).toEqual('draw');


        wrapper.setState(
            {
                result: 1
            }
        );
        expect(wrapper.instance().resolveStatus('p1')).toEqual('win');
        expect(wrapper.instance().resolveStatus('p2')).toEqual('lose');

        wrapper.setState(
            {
                result: 2
            }
        );
        expect(wrapper.instance().resolveStatus('p2')).toEqual('win');
        expect(wrapper.instance().resolveStatus('p1')).toEqual('lose');


        let error
        try {
            wrapper.instance().resolveStatus('p0')
        } catch (e) {
            error = e
        }
        expect(error).toBeInstanceOf(Error)

        try {
            wrapper.instance().resolveStatus('m')
        } catch (e) {
            error = e
        }
        expect(error).toBeInstanceOf(Error)

        try {
            wrapper.setState(
                {
                    result: 7
                }
            );
        } catch (e) {
            error = e
        }
        expect(error).toBeInstanceOf(Error)


    });
})